# ssg.sh - static site generator in a shell

I created this cursed shell script as part of a private project to convert a
directory full of Markdown files into a directory full of HTML files, including
an index file.

In the end, however, this script was too ugly to gather dust in a private
repository. Use at your own risk!

The site that is generated from the `./src` directory can be viewed
[here](https://blabber.codeberg.page/ssg.sh).

The stylesheet used is [MVP.css](https://github.com/andybrewer/mvp).
