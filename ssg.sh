#!/bin/sh

# "THE BEER-WARE LICENSE" (Revision 42):
# <tobias.rehbein@web.de> wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer in return. Tobias Rehbein

# Activate Bash Strict Mode
set -euo pipefail

MARKDOWN='markdown_py'
SRCDIR='./src'
OUTDIR='./out'

CSS='./css/mvp.css'

TITLE='The Title'

IFS='
'

process_dir() {
	local _dir _prefix _trail
	_dir="${1}"
	_prefix="${2}"
	if [ $# -lt 3 ] || [ -z "${3}" ]; then
		_trail="./"
	else
		_trail="${3}${_dir}/"
	fi

	local _line
	(cd "${_dir}" &&
		find . -mindepth 1 -maxdepth 1 -type d \! -path './.git' |
			sort |
			cut -d '/' -f 2 - | while read _line; do
				printf '%s %s\n' "${_prefix}" $(basename ${_line})
				process_dir "${_line}" "    ${_prefix}" "${_trail}"
			done &&

		find . -mindepth 1 -maxdepth 1 -type f -name '*.md'  \! -path './.git' \! -name 'index.md' |
			sort |
			cut -d '/' -f 2 - | while read _line; do
				local _name _link
				_name=$(echo "${_line}" | sed 's/\.md$//')
				_link="${_trail}${_line}"
				printf '%s [%s](%s)\n' "${_prefix}" $(basename "${_name}") "${_link}"
			done)
}

mkdir -p "${OUTDIR}"
cp -r $(dirname "${CSS}") "${OUTDIR}"

printf '# %s\n\n' "${TITLE}" > "${SRCDIR}/index.md"
process_dir "${SRCDIR}" "*" >> "${SRCDIR}/index.md"

for f in $(find "${SRCDIR}" -type f -name '*.md'); do
	OF=$(echo "${f}" | sed -e "s:^${SRCDIR}/:${OUTDIR}/:" -e 's:\.md$:.html:')
	mkdir -p $(dirname "${OF}")

	PAGETITLE="${TITLE} - $(basename "$f" | sed 's:\.md$::')"

	CSS_HREF=$(dirname "${OF}" | sed -e 's:^\./::' -e "s:^${OUTDIR}::" -e 's:[^/]*::g')
	CSS_HREF=$(echo "${CSS_HREF}" | sed 's:/:../:g')
	if [ -z "${CSS_HREF}" ]; then
		CSS_HREF='./'
	fi
	CSS_HREF="${CSS_HREF}${CSS}"

	cat > "${OF}" <<-EOF
		<!DOCTYPE html>
		<html>
		<head>
		    <link rel="stylesheet" href="${CSS_HREF}">
		
		    <meta charset="utf-8">
		    <meta name="viewport"
		          content="width=device-width, initial-scale=1.0">
		
		    <title>${PAGETITLE}</title>
		</head>
		
		<body>
		    <main>
	EOF

	sed 's:\.md\>:.html:' < "$f" | "${MARKDOWN}" >> "${OF}"

	cat >> "${OF}" <<-EOF

		    </main>
		</body>
		</html>
	EOF
done
